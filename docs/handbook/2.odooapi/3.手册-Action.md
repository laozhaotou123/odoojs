### 读取菜单数据

1. 登录后, 已经获得菜单数据
2. 直接调用同步函数, 获得菜单数据

```
import api from '@/odooapi'
const menu_data = api.web.menu_data
```

#### 返回结果

```
{
    name: "root",
    children:[
        {
            name: "联系人",
            web_icon:'contacts,static/description/icon.png',
            children: [
                {
                    name: "联系人",
                    action:'ir.actions.act_window,114',
                },
                {
                    name: "基础配置",
                    children: [
                        {
                            name: "联系人标签",
                            action: ir.actions.act_window,58'
                        },
                        {
                            name: "联系人称谓",
                            action: ir.actions.act_window,54'
                        },
                    ]
                }
            ]
        }
    ]
}
```

### Action

#### Action.load 接口

1. 接口: api.Action.load
2. 参数: action_xml_id. string 或 integer
3. 返回: action 信息

```
import api from '@/odooapi'
const test_action_with_xml_id = async () {
    const action_xml_id = 'contacts.action_contacts'
    const action = await api.Action.load(action_xml_id)
    console.log(action)
}

const test_action_with_id = async () {
    const action_xml_id = 'contacts.action_contacts'
    const action_ref = await api.env.ref(action_xml_id)
    const action_id = action_ref.id
    const action = await api.Action.load(action_id)
    console.log(action)
}
```

##### Action.load 返回结果

```
{
    id: 114,
    xml_id: "contacts.action_contacts",
    display_name: "联系人",
    name: "联系人",
    res_model: "res.partner",
    domain: false,
    context: "{'default_is_company': True}",
    view_mode: "kanban,list,form,activity",
    views: [
        [121, 'kanban'],
        [115, 'list'],
        [118, 'form'],
        [false, 'activity']
    ],
    type: "ir.actions.act_window",
    target: "current",
    search_view_id: [120, "res.partner.select"],
}
```

#### Action.load_views 接口

1. 接口: api.Action.load_views
2. 参数: {action}
3. 返回: views

```
import api from '@/odooapi'
const test_load_acton_and_views = async () {
    const action_xml_id = 'contacts.action_contacts'
    const action = await api.Action.load(action_id)
    const views = await api.Action.load_views({action})
}

```

##### Action.load_views 返回结果

```
{
    fields: {
        name: {
            string: "名称",
            type: "char",
            store: true,
            readonly: false,
        },
        // ...
    },
    fields_views: {
        list: {
            type: "tree",
            fields: { ... },
            arch: "<tree>...</tree>",
            toolbar: {
                print: [],
                action: []
            }
        },
        search: {...},
        form: {...}
    }
}
```

#### Action.view_mode 接口

1. 接口: api.Action.view_mode
2. 参数: {action, views}
3. 返回: ['kanban','list']
   该 action 可用的 view. kanban, list, pivot, graph
4. 为同步函数

```
import api from '@/odooapi'
const test = async () {
    const action_xml_id = 'contacts.action_contacts'
    const action = await api.Action.load(action_id)
    const views = await api.Action.load_views({action})
    const view_mode = api.Action.view_mode({action, views})
}
```
